package org.example.auth.jwt.config;

import groovy.util.logging.Slf4j;
import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.auth.jwt.service.JwtService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthRequestFilter extends OncePerRequestFilter {

    public static final String AUTHORITIES_CLAIM = "authorities";
    public static final String BEARER = "Bearer";
    public static final String AUTH_HEADER = "Authorization";
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        Optional<Authentication> authOptional = authenticate(httpServletRequest.getHeader(AUTH_HEADER));
        authOptional.ifPresent(auth -> SecurityContextHolder.getContext().setAuthentication(auth));
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private Optional<Authentication> authenticate(String authHeader) {
        final Optional<String> bearer = getBearer(authHeader);
        if (bearer.isEmpty()) {
            return Optional.empty();
        }

        final Claims claims = jwtService.parseToken(bearer.get());
        final Collection<? extends GrantedAuthority> userAuthorities = getUserAuthorities(claims);
        final UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(claims.getSubject(), "", userAuthorities);
        return Optional.of(authenticationToken);
    }

    private Optional<String> getBearer(String header) {
        if (header == null || !header.startsWith(BEARER)) {
            return Optional.empty();
        }

        final String jwt = header.substring(BEARER.length())
                .trim();
        return Optional.ofNullable(jwt);
    }

    private Collection<? extends GrantedAuthority> getUserAuthorities(Claims claims) {
        List<?> roles = claims.get(AUTHORITIES_CLAIM, List.class);
        return roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
    }


}

