package org.example.auth.jwt.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;

@Getter
@Configuration
@ConfigurationProperties(prefix = "security-jwt")
@RequiredArgsConstructor
public class JwtTokenConfigProperties {

    private final JwtProperties jwtProperties = new JwtProperties();

    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    @Setter
    @Component
    public static class JwtProperties {

        private String secret;
        private long tokenValidityInSeconds;
        private long refreshTokenValidityInSeconds;
    }

}
