package az.ingress.aspects;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EmployeeAspect {

    @Before("execution( public String getNameEmployee())")
    public void getNameAdvice() {
        System.out.println("Executing Advice on getNameEmployee()");
    }

    @Before("execution(* com.journaldev.spring.service.*.get*())")
    public void getAllAdvice() {
        System.out.println("Service method getter called");
    }

}