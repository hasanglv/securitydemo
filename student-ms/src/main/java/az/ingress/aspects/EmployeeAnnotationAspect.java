package az.ingress.aspects;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EmployeeAnnotationAspect {

    @Before("@annotation(az.ingress.aspects.Loggable)")
    public void myAdvice(){
        System.out.println("Executing myAdvice!!!");
    }
}