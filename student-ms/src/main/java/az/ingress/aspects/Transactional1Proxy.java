package az.ingress.aspects;


import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Transactional1Proxy {
    @SneakyThrows
    @Around("@annotation(az.ingress.aspects.Transactional1)")
    public void myAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("Transaction started!!!");
        try {
            proceedingJoinPoint.proceed();

        } catch (RuntimeException runtimeException) {
            System.out.println("Rollback transaction");
            throw runtimeException;
        } catch (Exception exception) {
            System.out.println("Commit transaction");
            throw exception;
        }

        System.out.println("Transaction finished!!!");

    }
}