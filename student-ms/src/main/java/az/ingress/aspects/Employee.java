package az.ingress.aspects;

import org.springframework.stereotype.Component;

@Component
public class Employee {

    private String nameEmployee;

    public String getNameEmployee() {
        return "Hasan";
    }

    @Loggable
    public void setNameEmployee(String nm) {
        System.out.println("Set employee name " + nm);
        this.nameEmployee = nm;
    }

    public void printEmployee() {
        System.out.println("PRINT EMPLOYEE" + getNameEmployee());
    }

}