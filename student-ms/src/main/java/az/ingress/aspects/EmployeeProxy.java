package az.ingress.aspects;

public class EmployeeProxy extends Employee {

    @Override
    public String getNameEmployee() {
        System.out.println("Method before advice");
         final String nameEmployee = super.getNameEmployee();
        System.out.println("Method before advice");
        return nameEmployee;
    }
}
