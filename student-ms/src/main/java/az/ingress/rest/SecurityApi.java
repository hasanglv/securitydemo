package az.ingress.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class SecurityApi {

    @GetMapping("/public1")
    public String getPublic() {
        return "Hello world,public api";
    }

    @GetMapping("/authenticated1")
    public String getAuthenticated() {
        return "Hello world,authenticated api";
    }

    @GetMapping("/role-user1")
    public String getRoleUser() {
        return "Hello world,authenticated and requires user role";
    }


    @GetMapping("/role-admin1")
    public String getRoleAdmin() {
        return "Hello world,authenticated and requires admin role";
    }

}
