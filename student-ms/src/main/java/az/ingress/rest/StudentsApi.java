package az.ingress.rest;

import az.ingress.domain.StudentEntity;
import az.ingress.dto.StudentRequestDto;
import az.ingress.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentsApi {
    private final StudentService studentService;

    @PostMapping
    public StudentEntity createStudent(@Validated @RequestBody
                                       StudentRequestDto studentRequestDto) {
        log.info("Received create student request {}", studentRequestDto);
        StudentEntity studentEntity =
                StudentEntity.builder()
                        .firstName(studentRequestDto.getFirstName())
                        .lastName(studentRequestDto.getLastName())
                        .build();
        return studentService.createStudent(studentEntity);
    }
//
//    @GetMapping
//    @SneakyThrows
//    @Lock(LockModeType.READ)
//    public List<StudentListResponse> studentEntityList() {
//        List<StudentListResponse> list = studentService.list();
//        Thread.sleep(10_000);
//        return list;
//    }


}
