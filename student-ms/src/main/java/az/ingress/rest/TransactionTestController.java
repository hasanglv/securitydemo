package az.ingress.rest;

import az.ingress.domain.BookEntity;
import az.ingress.domain.StudentEntity;
import az.ingress.dto.StudentResponseDto;
import az.ingress.repository.BookRepository;
import az.ingress.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test-control")
@RequiredArgsConstructor
@Slf4j
public class TransactionTestController {

    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;



    @GetMapping
    public StudentResponseDto get() {
        BookEntity book = BookEntity.builder()
                .bookName("SE1")
                .isbn("1")
                .build();
        BookEntity book2 = BookEntity.builder()
                .bookName("SE2")
                .isbn("1")
                .build();

        BookEntity book3= BookEntity.builder()
                .bookName("SE3")
                .isbn("1")
                .build();

        StudentEntity studentEntity = StudentEntity
                .builder()
                .firstName("Ilqar")
                .lastName("Hamidov")
                .age(22)
                .fatherName("Mamed")
                .build();

        book.setStudent(studentEntity);
        book2.setStudent(studentEntity);
        book3.setStudent(studentEntity);

        studentEntity.setBookEntities(List.of(book,book2,book3));

        studentRepository.save(studentEntity);

        return modelMapper.map(studentEntity, StudentResponseDto.class);

//        final BookEntity bookEntitySaved = bookRepository.save(book);
//
//        Thread.sleep(60000);
//        log.info("Book saved {}", bookEntitySaved);
//        return bookEntitySaved;
    }
}
