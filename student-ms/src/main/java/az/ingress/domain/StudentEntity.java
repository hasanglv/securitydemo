package az.ingress.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Entity
@Table(name = "students")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student",
            cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<BookEntity> bookEntities;

    private String firstName;

    private String lastName;

    private String studentNumber;

    @OneToOne(cascade = CascadeType.PERSIST) //ONE TO ONE
    private PassportEntity passport;

    private Integer age;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)    //MANY TO MANY
    private List<TeacherEntity> teacherEntities;

    @OneToOne(cascade = CascadeType.PERSIST)
    private AddressEntity address;

    private String fatherName;

    private String occupation;

    private String school;


}
