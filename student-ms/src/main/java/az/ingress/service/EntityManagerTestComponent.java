package az.ingress.service;

import az.ingress.domain.BookEntity;
import az.ingress.repository.BookRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class EntityManagerTestComponent {
    private final BookRepository bookRepository;
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

//    public EntityManager getEntityManager() {
//        return entityManager;
//    }

    public void saveBook() {
        bookRepository.save(BookEntity.builder()
                .isbn("TEST")
                .bookName("Hary222")
                .build());
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        BookEntity bookEntity = entityManager.find(BookEntity.class, 1L);
        bookEntity.setBookName("Marvel 333");
        transaction.commit();

    }
}
