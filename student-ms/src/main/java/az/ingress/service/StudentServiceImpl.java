package az.ingress.service;

import az.ingress.domain.StudentEntity;
import az.ingress.domain.TeacherEntity;
import az.ingress.dto.StudentListResponse;
import az.ingress.repository.StudentRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    @SneakyThrows
//    @Transactional
    public StudentEntity createStudent(StudentEntity studentEntity) {
//        Thread.sleep(1000);

        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setName("Test");
        studentEntity.setTeacherEntities(List.of(teacherEntity));
        final StudentEntity save = studentRepository.save(studentEntity);
//        Thread.sleep(10000);
        return save;
    }

    @Override
    @Transactional
    public List<StudentListResponse> list() {
       return   studentRepository.findAll()
                 .stream()
                 .map(studentEntity -> modelMapper.map(studentEntity,StudentListResponse.class))
                 .toList();
    }

    @Override
    public StudentEntity get(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("There is no student with this id"));
    }

    @Override
    public StudentEntity update(Long id, StudentEntity studentEntity) {
        studentEntity.setId(id);
        return studentRepository.save(studentEntity);
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }
}
