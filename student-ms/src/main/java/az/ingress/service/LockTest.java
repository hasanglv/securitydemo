package az.ingress.service;

import az.ingress.domain.StudentEntity;
import az.ingress.repository.StudentRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LockTest {

    private final StudentRepository studentRepository;

    @Transactional
    @SneakyThrows
    //Lost update
    public void updateStudents() {
//        final List<StudentEntity> selectForUpdateStudentsWhereIdGreaterThan = studentRepository.selectForUpdateStudentsWhereIdGreaterThan(5L);
        List<StudentEntity> byIdLessThan = studentRepository.findByIdLessThan(5L);
        Thread.sleep(50000);
        System.out.println("Updating students");
        for (StudentEntity student : byIdLessThan) {
            System.out.println("Updating student" + student.getId());
            if (student.getAge() >= 10) {
                student.setAge(student.getAge() -10);
            }
        }
    }
}
