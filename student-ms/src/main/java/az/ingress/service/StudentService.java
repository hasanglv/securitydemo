package az.ingress.service;

import az.ingress.domain.StudentEntity;
import az.ingress.dto.StudentListResponse;

import java.util.List;

public interface StudentService {

    StudentEntity createStudent(StudentEntity studentEntity);

    List<StudentListResponse> list();

    StudentEntity get(Long id);

    StudentEntity update(Long id, StudentEntity studentEntity);

    void delete(Long id);


}