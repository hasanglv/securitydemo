package az.ingress.service;

import az.ingress.domain.BookEntity;
import az.ingress.repository.BookRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TransactionalTestService {

    private final BookRepository bookRepository;

    @Transactional(rollbackOn = Exception.class)
//    @SneakyThrows
    public void testTransaction()  throws Exception{

        BookEntity book = BookEntity.builder()
                .bookName("OCA")
                .isbn("we234")
                .build();

        BookEntity bookEntitySaved = bookRepository.save(book);
        bookEntitySaved.setBookName("OSE");
        bookEntitySaved.setIsbn("re214");
        bookRepository.save(book);
    }



    //    @SneakyThrows
//    @Transactional
    private void test1() throws Exception {

    }


}
