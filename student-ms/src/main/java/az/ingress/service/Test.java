package az.ingress.service;

import lombok.SneakyThrows;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class Test {
    @SneakyThrows
    public static void main(String[] args) {

        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        Object object = new Object();
        PhantomReference<Object> phantomReference = new PhantomReference<>(object, referenceQueue);
        System.out.println("Before GC POLL " + referenceQueue.poll());
        System.out.println("Before GC ENQUE " + phantomReference.enqueue());
        object = null;
        System.out.println("Phantom reference  " + referenceQueue.poll());
        System.gc();
        Thread.sleep(1000);
        System.out.println("AFTER GC ENQUEUE " + phantomReference.enqueue());
        System.out.println("AFTER GC POLL " + referenceQueue.poll());

///////////////////////////////////////////////////////////////////////////////////
//        WeakReference<Object> weakReference = new WeakReference<>(new Object());
//        SoftReference<Object> softReference = new SoftReference<>(new Object());
//        System.out.println("Before gc weak " + weakReference.get());
//        System.out.println("Before gc soft " + softReference.get());
//
//        System.gc();
//        Thread.sleep(2000);
//
//        System.out.println("After gc weak " + weakReference.get());
//        System.out.println("After gc soft " + softReference.get());


    }
}
