package az.ingress.repository;

import az.ingress.domain.PassportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassportRepository extends JpaRepository<PassportEntity, Long>{
}
