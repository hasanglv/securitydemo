package az.ingress.repository;

import az.ingress.domain.StudentEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>,
        JpaSpecificationExecutor<StudentEntity> {
    List<StudentEntity> findByFirstNameAndLastName(String firstName, String lastName);

    List<StudentEntity> findByFirstNameLikeAndLastName(String firstName, String lastName);


    //JPQL
    @Query("SELECT s FROM StudentEntity s WHERE firstName =:firstName AND lastName =:lastName")
    List<StudentEntity> listStudentsJPQL(String firstName, String lastName);


    //NATIVE SQL
    @Query(nativeQuery = true, value = "SELECT s.* FROM students s WHERE first_name =:firstName AND last_name=:lastName")
    List<StudentEntity> listStudentsSQL(@Param("firstName") String firstName, String lastName);


    @Query(nativeQuery = true, value = "SELECT s.* FROM students s WHERE id>:id FOR UPDATE")
    List<StudentEntity> selectForUpdateStudentsWhereIdGreaterThan(Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<StudentEntity> findByIdLessThan(Long id);
}
