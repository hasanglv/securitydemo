//package az.ingress.user.details;
//
//import org.springframework.data.jpa.repository.EntityGraph;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Optional;
//
//public interface UserRepository extends JpaRepository<UserEntity,Long> {
//    @EntityGraph(attributePaths = UserEntity.Fields.authorities)
//    Optional<UserEntity> findByUsername(String username);
//
//}
