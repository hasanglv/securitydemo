package az.ingress.dto;

import lombok.*;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
public class BookResponseDto {

    private Long id;

    private String isbn;

    private String bookName;

    private StudentResponseDto student;
}
