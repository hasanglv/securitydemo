package az.ingress.dto;

import lombok.*;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
public class TeacherResponseDto {
    private Long id;

    private String name;


}
