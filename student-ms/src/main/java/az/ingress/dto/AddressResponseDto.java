package az.ingress.dto;

import lombok.*;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants

public class AddressResponseDto {
    private Long id;

    private String address;
}
