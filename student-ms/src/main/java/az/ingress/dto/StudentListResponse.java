package az.ingress.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentListResponse {
    private Long id;
    private List<BookResponseDto> bookEntities;

    private String firstName;

    private String lastName;

    private String studentNumber;

    private PassportResponseDto passport;

    private Integer age;

    private List<TeacherResponseDto> teacherEntities;

    private AddressResponseDto address;

    private String fatherName;

    private String occupation;

    private String school;
}
