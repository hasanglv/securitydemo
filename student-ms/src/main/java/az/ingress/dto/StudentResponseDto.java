package az.ingress.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
public class StudentResponseDto {

    private Long id;

    @JsonIgnore
    private List<BookResponseDto> bookEntities;

    private String firstName;

    private String lastName;

    private String studentNumber;

    private PassportResponseDto passport;

    private Integer age;

    private List<TeacherResponseDto> teacherEntities;

    private AddressResponseDto address;

    private String fatherName;

    private String occupation;

    private String school;

}
