package az.ingress;

import az.ingress.domain.StudentEntity;
import jakarta.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@EnableAspectJAutoProxy
public class StudentMs implements CommandLineRunner {
//    private final EmployeeService employeeService;
//    private final Employee employee;
//    private final TransactionalTestService testService;
//    private final AddressRepository addressRepository;
//    private final StudentService studentService;
//    private final StudentRepository studentRepository;
//    private final EntityManager entityManager;
//    private final TeacherRepository teacherRepository;
//
//    private final LockTest lockTest;
////    private final UserRepository userRepository;
////    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(StudentMs.class, args);
    }

    @Override
//    @Transactional
    public void run(String... args) throws Exception {

//
//        UserAuthority roleAdmin =
//                UserAuthority.builder()
//                        .authority("ROLE_ADMIN")
//                        .build();
//
//        UserEntity admin =
//                UserEntity
//                        .builder()
//                        .username("admin")
//                        .password(passwordEncoder.encode("1234"))
//                        .authorities(List.of(roleAdmin))
//                        .enabled(true)
//                        .accountNonExpired(true)
//                        .credentialsNonExpired(true)
//                        .accountNonLocked(true)
//                        .build();
//        roleAdmin.setUser(admin);
//
//        Optional<UserEntity> byUsername = userRepository.findByUsername(admin.getUsername());
//        if (byUsername.isEmpty()) {
//            userRepository.save(admin);
//        }


//        lockTest.updateStudents();

//        testService.testTransaction();

//        employee.setNameEmployee("TEST555");
        //        employee.printEmployee();

//        System.out.println(emp.getNameEmployee());
//        employeeService.printEmployee();
//        testService.testTransaction();


//        System.out.println("**********************CREATE Student********************");
//        System.out.println("The student succesfully created and added to db");
//        StudentEntity studentEntity =
//                StudentEntity.builder()
//                        .firstName("Kenan")
//                        .lastName("Chobanov")
//                        .fatherName("Ilham")
//                        .age(20)
//                        .build();
//        studentService.createStudent(studentEntity);
////        System.out.println("**********************LIST Student********************");
////        System.out.println(studentService.list());
////
////        System.out.println("************GET ONE*****************");
////        System.out.println(studentService.get(1L));
////        System.out.println(studentService.get(3l));
//
//        System.out.println("************UPDATE ONE*****************");
//        StudentEntity studentEntity1 =
//                StudentEntity.builder()
//                        .firstName("Kamran")
//                        .lastName("Xanmammedov")
//                        .age(21)
//                        .fatherName("Mahir")
//                        .build();
//        System.out.println(studentService.update(2L, studentEntity1));
//
////        System.out.println("************DELETE ONE*****************");
////        System.out.println("The student succesfully dleted");
////        studentService.delete(1L);
//
//        System.out.println("=================QUERY METHODS======================");
//        studentRepository.findByFirstNameAndLastName("Kenan", "Chobanov")
//                .stream()
//                .forEach(System.out::println);
//
//
//        System.out.println("=================QUERY METHODS  Like ======================");
//        studentRepository.findByFirstNameLikeAndLastName("Ken%  ", "Chobanov")
//                .stream()
//                .forEach(System.out::println);
//
//
//        System.out.println("==========******CRITERIA API=====******* ==========");
//        String firstName = "Kenan";
//        String lastName = "Chobanov";
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaQuery<StudentEntity> criteriaQuery = criteriaBuilder.createQuery(StudentEntity.class);
//        Root<StudentEntity> root = criteriaQuery.from(StudentEntity.class);
//        List<Predicate> list = new ArrayList<>();
//        if (firstName != null) {
//            Predicate firstNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.firstName), firstName);
//        }
//        if (firstName != null) {
//            Predicate flastNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.lastName), lastName);
//        }
//        criteriaQuery.select(root).where(list.toArray(new Predicate[0]));
//        TypedQuery<StudentEntity> typedQuery = entityManager.createQuery(criteriaQuery);
//        typedQuery.getResultList()
//                .stream()
//                .forEach(System.out::println);
//
////
////
//        System.out.println("********************************************");
//        studentRepository.findAll(listByFirstAndLastName(firstName, lastName))
//                .stream()
//                .forEach(System.out::println);
//
//
//        System.out.println("***************JPQL*************************");
//        studentRepository.listStudentsJPQL("Kenan", "Chobanov")
//                .stream()
//                .forEach(System.out::println);
//
//        System.out.println("***************NATIVE SQL*************************");
//        studentRepository.listStudentsSQL("Kamran", "Xanmammedov")
//                .stream()
//                .forEach(System.out::println);
////
////
//        System.out.println("***************PAGE REQUEST*************************");
//        studentRepository.findAll(PageRequest.of(4, 1))
//                .stream()
//                .forEach(System.out::println);
//
//        System.out.println("****************Lesson16 NEW******************");
//
//        PassportEntity passport =
//                PassportEntity.builder()
//                        .pin("12Bn3")
//                        .build();
//        Optional<AddressEntity> byId = addressRepository.findById(2L);
////        System.out.println("Address before"+ addressEntity);
//////        addressRepository.save(addressEntity);
////        System.out.println("Address after"+ addressEntity);
//
//        StudentEntity studentEntity2 =
//                StudentEntity.builder()
//                        .firstName("Dovlet")
//                        .lastName("Hamidov")
//                        .age(19)
//                        .fatherName("Ilqar")
//                        .passport(passport)
//                        .build();
//        studentRepository.save(studentEntity2);
//
//        BookEntity harryPotter =
//                BookEntity
//                        .builder()
//                        .isbn("ssssddda")
//                        .bookName("Harry Potter")
//                        .build();
//
//        BookEntity algorithms =
//                BookEntity
//                        .builder()
//                        .isbn("sdvdvdv")
//                        .bookName("Algorith and DS with JAVA")
//                        .build();

//        TeacherEntity kamran =
//                TeacherEntity.builder()
//                        .name("Kamran")
//                        .build();
//
//        TeacherEntity kenan =
//                TeacherEntity.builder()
//                        .name("Kenan")
//                        .build();
//
//        TeacherEntity kenan1 = teacherRepository.findById(2L).get();
//        kenan1.setName("Kenan 3");
//
//        TeacherEntity referenceById = teacherRepository.getReferenceById(2L);
//        referenceById.setName("Kenna ggg");
//
//        StudentEntity student =
//                StudentEntity.builder()
//                        .firstName("Atila")
//                        .lastName("Mammedov")
//                        .age(29)
//                        .fatherName("Habil")
//                        .passport(passport)
//                        .bookEntities(List.of(harryPotter,algorithms))
//                        .teacherEntities(List.of(kamran,kenan))
//                        .build();
//        harryPotter.setStudent(student);
//        algorithms.setStudent(student);
//
//        studentRepository.save(student);


//        AddressEntity addressEntity =
//                AddressEntity.builder()
//                        .address("Ziya Bunyadov")
//                        .build();
//
//
//        BookEntity harryPotter =
//                BookEntity
//                        .builder()
//                        .isbn("ssssddda")
//                        .bookName("Harry Potter")
//                        .build();
//
//        BookEntity algorithms =
//                BookEntity
//                        .builder()
//                        .isbn("sdvdvdv")
//                        .bookName("Algorith and DS with JAVA")
//                        .build();
//
//        TeacherEntity kamran =
//                TeacherEntity.builder()
//                        .name("Kamran")
//                        .build();
//
//        TeacherEntity kenan =
//                TeacherEntity.builder()
//                        .name("Kenan")
//                        .build();
//
////        TeacherEntity kenan1 = teacherRepository.findById(8L).get();
////        kenan1.setName("Kenan 3333");
//
//        TeacherEntity referenceById = teacherRepository.getReferenceById(8L);
//        referenceById.setName("Kenna gggjjj");
//
//
//        StudentEntity studentEntity =
//                StudentEntity.builder()
//                        .firstName("Malik")
//                        .lastName("Qurbanov")
//                        .age(22)
//                        .address(addressEntity)
//                        .bookEntities(List.of(harryPotter, algorithms))
//                        .teacherEntities(List.of(kamran, referenceById))
//                        .build();
//
//        harryPotter.setStudent(studentEntity);
//        algorithms.setStudent(studentEntity);
//        studentRepository.save(studentEntity);

//        Thread.sleep(10000);
//        System.out.println("*********************  LESSON19   **************************");
//        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        System.out.println("STARTED");
//        for (int i = 0; i < 100; i++) {
//            Future<List<StudentListResponse>> submit = executorService.submit(studentService::list);
//        }
//        System.out.println("COMPLETED");


    }

    public static Specification<StudentEntity> listByFirstNameAndLastName(String firstName, String lastName) {
        List<Predicate> list = new ArrayList<>();
        return ((root, query, criteriaBuilder) -> {
            if (firstName != null) {
                Predicate firstNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.firstName), firstName);
                list.add(firstNamePredicate);

            }
            if (lastName != null) {
                Predicate lastNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.lastName), lastName);
                list.add(lastNamePredicate);
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        });
    }

    public static Specification<StudentEntity> listByFirstAndLastName(String firstName, String lastName) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (firstName != null) {
                Predicate firstNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.firstName), firstName);
                list.add(firstNamePredicate);

            }
            if (lastName != null) {
                Predicate lastNamePredicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.lastName), lastName);
                list.add(lastNamePredicate);
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        };
//
    }

}
